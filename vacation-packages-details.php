<!-- Structure created by Kasi Emmanuel -->
<?php
    // Matt Jennings
    require('php/classes/package.class.php');
    require('php/classes/Customer.class.php');
    require('php/variables.php');

    $packageId = 0;
    $startDate="";
    $endDate="";
    $startDateFormatted="";
    $endDateFormatted="";
    $startDatePassed = false;
    $endDatePassed = false;

    $timeFormat = "Y-m-d H:i:s"; // This is the format that the date is stored in on the database

    if ($_GET['packageId'] != null) {
        $packageId = $_GET['packageId'];
    } else {
        header('Location: index.php');
    }


    $dbh =mysqli_connect($host, $user, $password, $database);

    //put connection checking here
    if (!$dbh)
    {
    print(mysqli_connect_error());
    }
    $sql = "SELECT `PkgName`, `PkgStartDate`, `PkgEndDate`, `PkgDesc`, `PkgBasePrice` FROM `packages` WHERE `packageId`=$packageId";
    $result = mysqli_query($dbh, $sql);
    //check if result is there
    if (!$result)
    {
    mysqli_error($dbh);
    }

    if ($row = mysqli_fetch_assoc($result)) {
        $package = new Package($row);

        $startDate = new DateTime($package->getStartDate());
        $endDate = new DateTime($package->getEndDate());
        $startDateFormatted = date_format($startDate, 'M d Y');
        $endDateFormatted = date_format($endDate, 'M d Y');


        // Turn the date into a number (seconds since january 1970) and compare it to today's time (also seconds since january 1970)
        // if $startDateFormatted is less than time(), then the date has passed
        if (strtotime($startDateFormatted) < time()) {
            $startDatePassed = true;
        }

        if (strtotime($endDateFormatted) < time()) {
            $endDatePassed = true;
        }

    } else {
        // Can't do much if we don't have a query return
        header('Location: index.php');
    }
    mysqli_close($dbh);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Order Details</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design Bootstrap -->
        <link href="css/mdb.min.css" rel="stylesheet">

        <!-- Custom styles -->
        <link href="css/style.css" rel="stylesheet">

        <link rel="stylesheet" href="css/footer.css">

        <script type="text/javascript" src="js/alert.js"></script>
    </head>

    <body>

        <!-- Modal Login -->
        <?php include('php/modal-login.php')
        ?>

        <!-- Modal Register -->
        <?php include('php/modal-register.php')
        ?>

        <!-- Modal Contact -->
        <?php include('php/modal-contact.php')
        ?>

        <!-- Modal Alert -->
        <?php include('php/modal-alert.php')
        ?>

        <!-- Must go before Navbar and after alert -->
        <?php
        require('php/loginMessageAlert.php');
        ?>

        <!-- Navbar -->

        <?php include('php/navbar.php')
        ?>
        <div class="main-content" id="package-details-container">
            <div class="container" id="order-details">
              <div class="col-lg-8">
                <h1>  <?php print($package->getName()) ?> </h1>

                <div class="row jumbotron" id="package-details-carousel">
                <!--Carousel Wrapper-->
                <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">

                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img src="img/jetty.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img src="img/scotland.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img src="img/sea.jpg" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img src="img/yosemite.jpg" alt="Fourth slide">
                        </div>
                    </div>
                    <!--/.Slides-->

                    <!--Thumbnails-->
                    <a class="left carousel-control" href="#carousel-thumb" role="button" data-slide="prev">
                        <span class="icon-prev" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-thumb" role="button" data-slide="next">
                        <span class="icon-next" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--/.Thumbnails-->

                    <ol class="carousel-indicators">
                        <li data-target="#carousel-thumb" data-slide-to="0" class="active"> <img src="" class="img-fluid"></li>

                        <li data-target="#carousel-thumb" data-slide-to="1"><img src="" class="img-fluid"></li>

                        <li data-target="#carousel-thumb" data-slide-to="2"><img src="" class="img-fluid"></li>
                        <li data-target="#carousel-thumb" data-slide-to="3"><img src="" class="img-fluid"></li>
                    </ol>
                </div>
                <!--/.Carousel Wrapper-->
            </div>

              <section class="jumbotron col-xs-12" id="package-details">
                <div class="row">
                    <section class="container">
                        <h3>Package Details</h3>
                        <p><?php print($package->getDescription()) ?></p>
                    </section>
			        	</div>

                <!-- Matt Jennings -->
                <div class="row">
                    <section class="col-xs-12">
                        <?php include("php/about-us-map.php");
                        print "<script>console.log($packageId)</script>";
                        print "<script>masterTravelExperts($packageId)</script>";
                        ?>
                    </section>
                </div>
                </section>

                <section class="jumbotron col-md-6 offset-md-0 col-xs-12" id="package-contents">
                    <h3>Includes</h3>
                    <ol>
                        <li>Lorem ipsum dolor sit amet, pri volutpat honestatis theophrastus eu, mei ut dicat etiam nusquam. Ius ei pertinax recteque, ad vidit pertinacia qui, quando reprehendunt cu duo.
                        </li>
                        <li> Ius ei pertinax recteque, ad vidit pertinacia qui, quando reprehendunt cu duo.
                        </li>
                        <li>Lorem ipsum dolor sit amet, pri volutpat honestatis theophrastus eu, mei ut dicat etiam nusquam.
                        </li>
                        <li>Ad vidit pertinacia qui, quando reprehendunt cu duo.
                        </li>
                    </ol>
				        </section>

                <section class="jumbotron col-md-5 offset-md-1 col-xs-12" id="pointers">
                    <h3>Travel Tips</h3>
                     <ol>
                      <li>Lorem ipsum dolor sit amet.
                      </li>
                      <li>Ius ei pertinax recteque quando reprehendunt cu duo.
                      </li>
                      <li>Heophrastus eu, mei ut dicat etiam nusquam.
                      </li>
                      <li>Ad vidit pertinacia qui, quando reprehendunt cu duo.
                      </li>
                     </ol>
				        </section>
			  </div>
            <!-- Order details - Matt Jennings -->
     <form action="BookingsConfirmation.php" id="package-form" method="post">
       <!-- Invisible input field,this is an easy way of passing the packageId along with the submit -->
       <input value="<?php print( $_GET['packageId'])?>" name="packageId" hidden="hidden"/>
      <div class="col-lg-4 col-xs-12">
                <div class="jumbotron" id="all-details">
                    <!-- Booking info -->
                    <section class="row" id="booking-info" style="min-height: 300px">
                        <h3 align="center" style="border-bottom: 1px solid black; padding-bottom: 5px;">Booking Details</h3>
                        <div class="col-xs-12">
                            <div class="booking-class">
                                <label>CLASS</label>
                                <br>
                                <div class="booking-class-select col-xs-6 offset-xs-3">
                                    <div class="material-select">
                                        <select name="TripTypeId">
                                            <option value="B">Business</option>
                                            <option value="G">Group</option>
                                            <option value="L">Leisure</option>
                                        </select>
                                    </div>
                            </div>
                        <div class="num-travelers col-xs-4 offset-xs-4">
                            <div class="num-travelers">
                                <p>Travelers</p>
                            </div>
                            <div class="travelers">
                                <div class="material-select">
                                    <select name="TravelerCount">
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="row" id="package-info" style="min-height:300px;">
                        <h3 align="center" style="border-bottom: 1px solid black; padding-bottom: 5px;">Package Details</h3>
                        <div class="col-xs-6">
                            <div class="package-start-date">
                                <label>START</label>
                                <p class=<?php print($startDatePassed ? "date-passed" : "date-ahead")?>><?php print($startDateFormatted) ?></p>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="package-end-date">
                                <label>END</label>
                                <p class=<?php print($endDatePassed ? "date-passed" : "date-ahead")?>><?php print($endDateFormatted) ?></p>
                            </div>
                        </div>
                        <div class="package-price col-xs-12">
                            <div class="price-label">
                                <p>PRICE</p>
                            </div>
                            <div class="price-number">
                                <p>$<?php print($package->getBasePrice()) ?></p>
                            </div>
                        </div>
                    </section>
                    <?PHP
                    // Matt Jennings
                    if (isset($_SESSION['CustomerId'])) {

                        $dbh =mysqli_connect($host, $user, $password, $database);

                        //put connection checking here
                        if (!$dbh)
                        {
                        print(mysqli_connect_error());
                        }
                        $sql = "SELECT `CustFirstName`, `CustLastName`, `CustAddress`, `CustCity`, `CustProv`, `CustPostal`, `CustCountry`, `CustHomePhone`, `CustBusPhone`, `CustEmail` FROM `customers` WHERE `CustomerId`=" . $_SESSION['CustomerId'];
                        $customerResult = mysqli_query($dbh, $sql);


                        //check if result is there
                        if (!$customerResult)
                        {
                            print(mysqli_error($dbh));
                            print("<script>console.log('". "blah" . "')</script>");
                        }

                        if ($row = mysqli_fetch_assoc($customerResult)) {
                            $customer = new Customer($row);

                            print("<script>console.log('". $customer->getFirstName() . "')</script>");
                        }

                        mysqli_close($dbh);
                        // Stores the options for the select input
                        $selectArray = array("AB", "BC", "SK", "MB", "ON", "QC");

                        // If you're confused by this, echo <<<EOT is just another way of doing print(""), but it allows double quotes without having to escape them or use single quotes (called a Heredoc)
                        // A responding EOT; at the BEGINNING OF THE LINE will end the echo. EOT is arbitrary, it can be named anything. Like a variable.
                        echo <<<EOT
                        <section class="row" id="customer-info"style="min-height: 300px;">
                            <h3 align="center" style="border-bottom: 1px solid black; padding-bottom: 5px;">Customer Information</h3>
                            <div class="details-customer-fields">
                                <div class="col-xs-12 col-md-6">
                                    <div class="md-form">
                                        <input type="text" id="firstName" name="CustFirstName" class="form-control" value="{$customer->getFirstName()}" required="required"></input>
                                        <label for="details-cust-fname">First Name</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="md-form">
                                        <input type="text" id="lastName" name="CustLastName" class="form-control" value="{$customer->getLastName()}" required="required"></input>
                                        <label for="details-cust-lname">Last Name</label>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-12">
                                    <div class="md-form">
                                        <input type="text" id="address" name="CustAddress" class="form-control" value="{$customer->getAddress()}" required="required"></input>
                                        <label for="details-cust-address">Address</label>
                                    </div>

                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="md-form">
                                        <input type="text" id="city" name="CustCity" class="form-control" value="{$customer->getCity()}" required="required"></input>
                                        <label for="details-cust-city">City</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 material-select " id="CustProv" name="CustProv">
                                <select>
EOT;
                                // Escape the echo so we can run some calculations

                                // Have the default option selected be the customer's province
                                foreach($selectArray as $sel) {
                                    if($sel == $customer->getProvince()){
                                        echo "<option selected='selected' value='$sel'>$sel</option>";
                                    }else{
                                        echo "<option value='$sel'>$sel</option>";
                                    }
                                }

                                // ReGEX by Kasi Emmanuel
                                // Continue echoing the rest of the form
                                echo <<<EOT
                                </select>
                                </div>
                                <div class="col-xs-12">
                                    <div class="md-form">
                                        <input type="text" id="postalCode" name="CustPostal" class="form-control" value="{$customer->getPostalCode()}" required="required"></input>
                                        <label for="details-cust-Postal">Postal Code</label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="md-form">
                                        <input type="text" id="country" name="CustCountry" class="form-control" value="{$customer->getCountry()}" required="required"></input>
                                        <label for="details-cust-country">Country</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 md-6">
                                    <div class="md-form">
                                        <input type="tel" pattern="^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$"  required="required" id="homePhone" name="CustHomePhone" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="{$customer->getHomePhone()}" maxlength="12"></input>
                                        <label for="details-cust-home-phone">Home Phone(xxx-xxx-xxxx)</label>
                                    </div>
                                </div>
                                <div class="col-xs-12 md-6">
                                    <div class="md-form">
                                        <input type="tel" pattern="^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$" id="busPhone" name="CustBusPhone" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="{$customer->getBusPhone()}" maxlength="12"></input>
                                        <label for="details-cust-bus-phone">Business Phone(xxx-xxx-xxxx)</label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="md-form">
                                        <input type="email" id="email" name="CustEmail" class="form-control" value="{$customer->getEmail()}"></input>
                                        <label for="details-cust-email">Email</label>
                                    </div>
                                </div>
                            </div>
                            <!-- Created by Kasi Emmanuel -->
                            <!-- <a href="index.php" id="bookingsPreviousBtn"  role="button" class="btn btn-default">Back To Search</a> -->
                            <div class='text-xs-center'>
                                <button type="submit" id="bookingsConfirmBtn" class="btn btn-default">Book Now </button>
                            </div>
EOT;
                    }
                    else {
                        print("
                        <div class='text-xs-center'>
                        <p>Please login to purchase a package</p>
                        <a class='btn btn-primary' data-toggle='modal' data-target='#modal-login'>Login</a>
                        <br>
                        </div>
                        ");
                    }
                    ?>

                    </section>
                    </form>
                </div>
			</div>
		</div>
        </div>
        <?php
            require("php/footer.php");
        ?>

        <!-- SCRIPTS -->

        <!-- JQuery -->
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>

        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="js/tether.min.js"></script>

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="js/mdb.min.js"></script>

        <script type="text/javascript" src="js/main.js"></script>
    </body>

</html>
