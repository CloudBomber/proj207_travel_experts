<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Travel Experts</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design Bootstrap -->
        <link href="css/mdb.min.css" rel="stylesheet">

        <!-- Custom styles -->
        <link href="css/style.css" rel="stylesheet">

        <link rel="stylesheet" href="css/footer.css">

        <script type="text/javascript" src="js/alert.js"></script>
    </head>

    <body>

        <!-- Modal Login -->
        <?php include('php/modal-login.php')
        ?>

        <!-- Modal Register -->
        <?php include('php/modal-register.php')
        ?>

        <!-- Modal Contact -->
        <?php include('php/modal-contact.php')
        ?>

        <!-- Modal Alert -->
        <?php include('php/modal-alert.php')
        ?>

        <!-- Must go before Navbar and after alert -->
        <?php
        require('php/loginMessageAlert.php');
        ?>

        <!-- Navbar -->
        <?php include('php/navbar.php')
        ?>

        <?php
        // Welcome message done by Kasi, PHP time check done by Matt


        if (!isset($_SESSION['lastTimeWelcomed'])) {
            $_SESSION['lastTimeWelcomed'] = time();
        }

        $currentTime = time();
        $difTime = $_SESSION['lastTimeWelcomed'] - time();

        // If it's been >60 seconds or the first time loading the page, display the welcome message
        if ($difTime < -60 || $difTime >= -1) {
            $_SESSION['lastTimeWelcomed'] = time();
            print("<div class='welcome-message z-depth-1' id='wel-note' style='background-color:#00796B;width:100%;position:absolute; top:-60px;z-index:100;'><h1 align='center' style='color:#e0e8eb;'>Welcome to Travel Experts</h1></div>");
        }
        ?>

        <!-- Home Page -->
        <?php include('php/home.php')
         ?>

        <div class="main-content">
            <!-- Vacation Page - Created by Kasi Emmanuel -->
            <?php include 'php/packages.php'
            ?>

            <!-- Show map and all travel lines -->
            <div class="container" id="about-us-map">
                <div class="row">
                    <div class="col-xs-12 offset-xs-0 col-md-8 offset-md-2 col-lg-10 offset-lg-1">
                        <?php include 'php/about-us-map.php'
                        ?>
                        <script>rotateThroughMaster()</script>
                    </div>
                </div>
            </div>


            <!-- About Us Page -->
            <?php include 'php/about-us.php'
            ?>

            <?php
                require("php/footer.php");
            ?>
        </div>

        <!-- SCRIPTS -->

        <!-- JQuery -->
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>

        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="js/tether.min.js"></script>

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="js/mdb.min.js"></script>

        <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>
