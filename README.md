Travel Experts PROJ207
=========

A group project for the Fall 2016 OOSD course where we design a website for a fictional client.

### Table of Contents
* [Documentation](#documentation)
* [Other Resources](#other-resources)

# Documentation

* [Bootstrap](http://getbootstrap.com/)
* [Material Design Bootstrap Theme](http://mdbootstrap.com/) - This is the Material Design theme that we'll be utilizing. Use this for reference to various components and styling.

# Other Resources

* [Google Material Design](https://material.google.com/) - Google's official Material design guide
* [Google Color Palette](https://material.google.com/style/color.html#color-color-palette) - Colors recommended by Google
* [The Noun Project](https://thenounproject.com/) - Hundreds of thousands of icons. I have a subscription to this website so we can use any icon on there without having to give credit to the author.

