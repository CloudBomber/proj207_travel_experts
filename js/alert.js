/* Matt Jennings
   
   Needs to be included at top of page so that loginMessageAlert.php, which is included further down the page, can call showAlert   
*/

// Waits 400ms so that jquery and other libraries are initialized as this function is called upon index page load. 
// If we don't wait 400ms then it won't be able to use jQuery. jQuery is initialized at the end of index.php, while this is at the top, with loginMessageAlert in the middle. It's dumb.
function showAlert(title, msg) {  
    setTimeout(function() {
        $('#modal-alert').modal('show');
        $('#modal-alert-title').html(title);
        $('#modal-alert-text').html(msg);
    }, 400);  
}