/**
 *  This runs any custom activities for the website
 *
 *  Created by Matt Jennings, Kasi & Justin contributed functions toward the end
 *
 */

/* Matt Jennings */

/* This function runs at startup */
$(function(){
    /* Collapse navbar menu on mobile when link is clicked */
    var navMain = $("#collapseEx");
    navMain.on("click", "a", null, function (e) {
		if (e.currentTarget.id != "aboutUsDropDown") {
        	navMain.collapse('hide');
        	$('#nav-icon4').toggleClass('open');
		}
    });

    /* Toggle menu animation */
    $('#nav-icon4').click(function(){
        $(this).toggleClass('open');
    });

	// Contact us submit button
	$('#contact-us-form').submit(function(ev) {
		// Cacnel the submit function since it's just a dummy page right now
    	ev.preventDefault();
    	showContactConfirmation('Success', 'Thank you for contacting us. An agent will be in touch with you shortly.<br><br> Have a great day!')
	});
 });


// Hides navbar when scrolling down on mobile, shows it when scrolling up
/* disabled for now
var lastScrollTop = 0;
var navbar        = $('.navbar');
$(window).scroll(function(event){	
   var st = $(this).scrollTop();

   if (st > lastScrollTop && st > 0){
       navbar.addClass('navbar-scroll-custom');
   } else {
      navbar.removeClass('navbar-scroll-custom');	  
   }
   lastScrollTop = st;
}); 
*/

/* Cancel scrollToId if user tries to scroll manually */
$('body,html').bind('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function(e){
	if ( e.which > 0 || e.type == "mousedown" || e.type == "mousewheel" || e.type == "touchmove"){
		$("html,body").stop();
	}
})		

/* Animate a scroll to the id */
function scrollToId(id) {
	  var offset = -50;
	  
	  if ($(window).width() < 768) {
		  offset = 0;
	  }
      $(document).ready(function() {
         $('html, body').animate({
           'scrollTop':   $(id).offset().top + offset
         }, 'slow');
    });
}

// Used in modal-contact.php to filter the agency branches.
// agencyButton is a document element
function showAgency(agencyButton) {
    if ($(agencyButton).attr('id') == "showCalgaryBtn") {
        $('.agency-calgary').css({"display": "block"});
        $('.agency-okotoks').css({"display": "none"});
        $('#showCalgaryBtn').toggleClass('active', true);
        $('#showOkotoksBtn').toggleClass('active', false);
    } else {
        $('.agency-calgary').css({"display": "none"});
        $('.agency-okotoks').css({"display": "block"});
        $('#showCalgaryBtn').toggleClass('active', false);
        $('#showOkotoksBtn').toggleClass('active', true);
    }
}

// Call this at the start so Calgary agency is shown by default
showAgency(document.getElementById('showCalgaryBtn'));

// When a user clicks on a package to book, this loads up vacation-packages-details.php with the correct packageId
function goToPackageDetails(packageId) {
    window.open('vacation-packages-details.php?packageId=' + packageId, "_self");
}

// Workaround for a bug in bootstrap that would remove the scrollbar if you opened a new modal immediately after closing one
function openModalDelayed(openModal) {
	setTimeout(function () {
        $(openModal).modal('show');
    }, 600);
}

// Shows an alert
// Takes 2 parameter of types String, which is the title & the message to display in the alert
function showContactConfirmation(title, msg) {	
	$('#modal-contact').modal('hide');
	$('#modal-alert').modal('show');
	$('#modal-alert-title').html(title);
	$('#modal-alert-text').html(msg);
}

/* End of Matt Jennings' functions */

/* Justin Pilon */
function registerValidate()
{

	var stat = true;
	var field1 = document.getElementById("customerFirstName");
  var field2 = document.getElementById("customerLastName");
  var field3 = document.getElementById("customerAddress");
  var field4 = document.getElementById("customerEmail");
	var field5 = document.getElementById("customerBusPhone");
  var field6 = document.getElementById("regUserId");
  var field7 = document.getElementById("customerHomePhone");
  var field8 = document.getElementById("customerPassword");
  var regexpr1 =  /[A-Z0-9_.-]+@[A-Z0-9]+[.][A-Z0-9]+/i;
  var field9 = document.getElementById("customerEmail");

	if (field1.value == "")
	{
		alert("Must enter your first name");
		field1.focus();
		stat = false;
	}
	else if (field2.value == "")
	{
		alert("Must enter your last name");
		field2.focus();
		stat = false;
	}
	else if (field3.value == "")
	{
		alert("Must enter your address");
		field3.focus();
		stat = false;
	}
	else if (field4.value == "")
	{
		alert("Must enter your Email");
		field4.focus();
		stat = false;
	}
	/* Business phone number is optional -- Matt J
	else if (field5.value == "")
	{
		alert("Must enter your business phone number");
		field5.focus();
		stat = false;
	}
	*/
	else if (field6.value == "")
	{
		alert("Must enter your Id");
		field6.focus();
		stat = false;
	}
	else if (field7.value == "")
	{
		alert("Must enter your home phone #");
		field7.focus();
		stat = false;
	}
	else if (field8.value == "")
	{
		alert("Must enter your password");
		field8.focus();
		stat = false;
	}
	else if ((regexpr1.test(field9.value) != true))
	{
		alert("Must be a valid Email address");
		field9.focus();
		stat = false;
	}
	return stat;
}

/* Start of Kasi Emmanuel functions */

$(function(){
    $('.welcome-message').fadeIn(2000).animate({"top":"20px"},1000, function() {

        setTimeout(function() {
          $('.welcome-message').animate({"top":"-50px"},1000);
        }, 2000);

    });

});

/* End of Kasi Emmanuel functions  */
