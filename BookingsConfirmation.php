<?php
    /*
        Created by Matt Jennings

        This php file is used to submit the booking information from vacation-packages-details.php into the database
    */

    session_start();    
    require('php/classes/package.class.php');
    require('php/variables.php');

    $packageId = 0;
    $startDate="";
    $endDate="";
    $startDateFormatted="";
    $endDateFormatted="";
    $startDatePassed = false;
    $endDatePassed = false;

    $timeFormat = "Y-m-d H:i:s"; // This is the format that the date is stored in on the database

    $cancelOrder = false; // Cancel the order if true

    // Used to generate random booking number
    function generateRandomString($length = 6) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    // Create connection
    $conn = new mysqli($host, $user, $password, $database);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    $currentDate = date($timeFormat);
    $bookingNumber = generateRandomString(6);
    
    // The package id is sent along via post with an invisible input field
    if ($_POST['packageId'] != null) {
        $packageId = $_POST['packageId'];        
    } else {
        $cancelOrder = true;        
    }

    if (isset($_POST['TravelerCount'])) {        
        $travelerCount = $_POST['TravelerCount'];
    } else {
        $cancelOrder = true;
    }

    if (isset($_POST['TripTypeId'])) {
        $tripTypeId = $_POST['TripTypeId'];        
    } else {
        $cancelOrder = true;        
    }

    if (isset($_SESSION['CustomerId'])) {
        $customerId = $_SESSION['CustomerId'];
    } else {
        $cancelOrder = true;
    }
        
    if ($cancelOrder == false) {
        $sql = "INSERT INTO bookings (BookingDate, BookingNo, TravelerCount, CustomerId, TripTypeId, PackageId) VALUES ('$currentDate', '$bookingNumber', '$travelerCount', '$customerId', '$tripTypeId', '$packageId')";
        
        if ($conn->query($sql) === TRUE) {                
            $_SESSION['loginTitle'] = "Success";
            $_SESSION['loginMessage']="Your booking number is $bookingNumber. Please check your email for the full invoice.";                       
        } else {        
            $_SESSION['loginTitle'] = "Error";     
            $_SESSION['loginMessage']="There was a problem purchasing your order. Please contact one of our agents.";        
        }
    }  else {
        // Something went wrong while retrieving form data, give generic error for user to contact agent.        
        $_SESSION['loginTitle'] = "Canceled";     
        $_SESSION['loginMessage']="There was a problem purchasing your order. Please contact one of our agents." ;        
    }
    
    
    // Redirect to the previous page
    $pageId= $_SERVER['HTTP_REFERER'];
    print(" <script type='text/javascript'>
    window.location.href = '$pageId';
    </script>");
        
    mysql_close($conn);
?>