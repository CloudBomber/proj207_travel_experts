<!-- Created by Matt Jennings -->

<!-- Modal Alert -->
<div class="modal fade modal-ext" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="alert-header" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <!-- Text supplied by function -->
                <h4 class="modal-title" id="modal-alert-title"></h4>
            </div>

            <!--Body-->
            <div class="modal-body">          
                <!-- Text supplied by function -->      
                <p id="modal-alert-text"></p>                
            </div>
            <!--Footer-->

            <div class="modal-footer">
                <!-- /.Live preview -->
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
