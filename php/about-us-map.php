<!-- Created by Justin Pilon -->
<div class="">
	<div class="row">
		<div class="col-xs-12 pictures">
		<style>
			canvas {
				overflow: hidden;
				width: 95%;
				height: auto;
			}
		</style>

			<div class="row">
				<div class="col-md-10 offset-md-1 col-sm-12 offset-md-0">
					<!-- <img id="world" width="0" height="0" src="img/earth.png"> -->
				<center><canvas id="myCanvas" width=600px height=322px style="background: url('./img/earth.png') no-repeat;background-size:cover;"></canvas></center>
				</div>
			</div>

			<script>
			r = 700;

			var OttowX = 185, OttowY = 130, CalgaX = 120, CalgaY = 120, WashiX = 180, WashiY = 150;
			var SanFrX = 105, SanFrY = 150, BrasiX = 230, BrasiY = 250, LondoX = 310, LondoY = 120;
			var CanbeX = 555, CanbeY = 295, MoscoX = 365, MoscoY = 110, TokyoX = 540, TokyoY = 155;
			var BeijiX = 505, BeijiY = 145, ReykjX = 275, ReykjY = 85, AnchoX = 60, AnchoY = 90;
			var CairoX = 360, CairoY = 170, MexicX = 140, MexicY = 185;

			var CariOneX = 173, CariOneY = 183, CariTwoX = 180, CariTwoY = 186, CariThreeX = 190, CariThreeY = 190;
			var PolyOneX = 50, PolyOneY = 190, PolyTwoX = 621, PolyTwoY = 252, PolyThreeX = 592, PolyThreeY = 318;
			var AsiaOneX = 505, AsiaOneY = 145, AsiaTwoX = 538, AsiaTwoY = 155, AsiaThreeX = 480, AsiaThreeY = 195;
			var EuroOneX = 330, EuroOneY = 140, EuroTwoX = 312, EuroTwoY = 124, EuroThreeX = 305, EuroThreeY = 117;

			function oneWayTripPath()
			{
				var coordinatesInitial = [];
				if (document.getElementById("initial").selectedIndex == "0") {coordinatesInitial[0] = OttowX; coordinatesInitial[1] = OttowY;}
				if (document.getElementById("initial").selectedIndex == "1") {coordinatesInitial[0] = CalgaX; coordinatesInitial[1] = CalgaY;}
				if (document.getElementById("initial").selectedIndex == "2") {coordinatesInitial[0] = WashiX; coordinatesInitial[1] = WashiY;}
				if (document.getElementById("initial").selectedIndex == "3") {coordinatesInitial[0] = SanFrX; coordinatesInitial[1] = SanFrY;}
				if (document.getElementById("initial").selectedIndex == "4") {coordinatesInitial[0] = BrasiX; coordinatesInitial[1] = BrasiY;}
				if (document.getElementById("initial").selectedIndex == "5") {coordinatesInitial[0] = LondoX; coordinatesInitial[1] = LondoY;}
				if (document.getElementById("initial").selectedIndex == "6") {coordinatesInitial[0] = CanbeX; coordinatesInitial[1] = CanbeY;}
				if (document.getElementById("initial").selectedIndex == "7") {coordinatesInitial[0] = MoscoX; coordinatesInitial[1] = MoscoY;}
				if (document.getElementById("initial").selectedIndex == "8") {coordinatesInitial[0] = TokyoX; coordinatesInitial[1] = TokyoY;}
				if (document.getElementById("initial").selectedIndex == "9") {coordinatesInitial[0] = BeijiX; coordinatesInitial[1] = BeijiY;}
				if (document.getElementById("initial").selectedIndex == "10") {coordinatesInitial[0]= ReykjX; coordinatesInitial[1] = ReykjY;}
				if (document.getElementById("initial").selectedIndex == "11") {coordinatesInitial[0]= AnchoX; coordinatesInitial[1] = AnchoY;}
				if (document.getElementById("initial").selectedIndex == "12") {coordinatesInitial[0]= CairoX; coordinatesInitial[1] = CairoY;}
				if (document.getElementById("initial").selectedIndex == "13") {coordinatesInitial[0]= MexicX; coordinatesInitial[1] = MexicY;}
				var coordinatesFinal = [];
				if (document.getElementById("final").selectedIndex == "0") {coordinatesFinal[0] = OttowX; coordinatesFinal[1] = OttowY;}
				if (document.getElementById("final").selectedIndex == "1") {coordinatesFinal[0] = CalgaX; coordinatesFinal[1] = CalgaY;}
				if (document.getElementById("final").selectedIndex == "2") {coordinatesFinal[0] = WashiX; coordinatesFinal[1] = WashiY;}
				if (document.getElementById("final").selectedIndex == "3") {coordinatesFinal[0] = SanFrX; coordinatesFinal[1] = SanFrY;}
				if (document.getElementById("final").selectedIndex == "4") {coordinatesFinal[0] = BrasiX; coordinatesFinal[1] = BrasiY;}
				if (document.getElementById("final").selectedIndex == "5") {coordinatesFinal[0] = LondoX; coordinatesFinal[1] = LondoY;}
				if (document.getElementById("final").selectedIndex == "6") {coordinatesFinal[0] = CanbeX; coordinatesFinal[1] = CanbeY;}
				if (document.getElementById("final").selectedIndex == "7") {coordinatesFinal[0] = MoscoX; coordinatesFinal[1] = MoscoY;}
				if (document.getElementById("final").selectedIndex == "8") {coordinatesFinal[0] = TokyoX; coordinatesFinal[1] = TokyoY;}
				if (document.getElementById("final").selectedIndex == "9") {coordinatesFinal[0] = BeijiX; coordinatesFinal[1] = BeijiY;}
				if (document.getElementById("final").selectedIndex == "10") {coordinatesFinal[0]= ReykjX; coordinatesFinal[1] = ReykjY;}
				if (document.getElementById("final").selectedIndex == "11") {coordinatesFinal[0]= AnchoX; coordinatesFinal[1] = AnchoY;}
				if (document.getElementById("final").selectedIndex == "12") {coordinatesFinal[0]= CairoX; coordinatesFinal[1] = CairoY;}
				if (document.getElementById("final").selectedIndex == "13") {coordinatesFinal[0]= MexicX; coordinatesFinal[1] = MexicY;}
				var output = [coordinatesInitial[0],coordinatesInitial[1],coordinatesFinal[0],coordinatesFinal[1]];
				return output;
			}

			var c = document.getElementById("myCanvas");
			var ctx = c.getContext("2d");

			/*var img = document.getElementById("world");
			ctx.drawImage(img, 25, 25);*/

/*			var background = new Image();
			background.src = "img/earth.png";

			background.onload = function(){
				Console.log("BGLOAD");
			ctx.drawImage(background,0,0);
		}​;*/

			var xOffset = -25;
			var yOffset = -26;

			var i = 0;
			hexList = ["F","E","D","C","B","A","9","8","7","6","5","4","3","2","1","0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"];

			x = [(OttowX+xOffset),(CalgaX+xOffset),(WashiX+xOffset),(SanFrX+xOffset),(BrasiX+xOffset),(LondoX+xOffset),(CanbeX+xOffset),(MoscoX+xOffset),(TokyoX+xOffset),(BeijiX+xOffset),(ReykjX+xOffset),(AnchoX+xOffset),(CairoX+xOffset),(MexicX+xOffset)];
			y = [(OttowY+yOffset),(CalgaY+yOffset),(WashiY+yOffset),(SanFrY+yOffset),(BrasiY+yOffset),(LondoY+yOffset),(CanbeY+yOffset),(MoscoY+yOffset),(TokyoY+yOffset),(BeijiY+yOffset),(ReykjY+yOffset),(AnchoY+yOffset),(CairoY+yOffset),(MexicY+yOffset)];

			xCaribbean = [(CalgaX+xOffset),(CariOneX+xOffset),(CariTwoX+xOffset),(CariThreeX+xOffset)];
			yCaribbean = [(CalgaY+yOffset),(CariOneY+yOffset),(CariTwoY+yOffset),(CariThreeY+yOffset)];

			xPolynesian = [(CalgaX+xOffset),(PolyOneX+xOffset),(PolyTwoX+xOffset),(PolyThreeX+xOffset)];
			yPolynesian = [(CalgaY+yOffset),(PolyOneY+yOffset),(PolyTwoY+yOffset),(PolyThreeY+yOffset)];

			xAsian = [(CalgaX+xOffset),(AsiaOneX+xOffset),(AsiaTwoX+xOffset),(AsiaThreeX+xOffset)];
			yAsian = [(CalgaY+yOffset),(AsiaOneY+yOffset),(AsiaTwoY+yOffset),(AsiaThreeY+yOffset)];

			xEuropean = [(CalgaX+xOffset),(EuroOneX+xOffset),(EuroTwoX+xOffset),(EuroThreeX+xOffset)];
			yEuropean = [(CalgaY+yOffset),(EuroOneY+yOffset),(EuroTwoY+yOffset),(EuroThreeY+yOffset)];

			function colorChange()
			{
				for (var j = 0; j < x.length; j++)
				{
					point(x[j],y[j],hexList[i],hexList[i]);
				}
				if ( i > hexList.length)
				{
					i = 0;
				}
				i++;
				setTimeout("colorChange()",100);
			}

			function colorChangeCari()
			{
				for (var j = 0; j < xCaribbean.length; j++)
				{
					point(xCaribbean[j],yCaribbean[j],hexList[i],hexList[i]);
				}
				if ( i > hexList.length)
				{
					i = 0;
				}
				i++;
				setTimeout("colorChangeCari()",100);
			}

			function colorChangePoly()
			{
				for (var j = 0; j < xPolynesian.length; j++)
				{
					point(xPolynesian[j],yPolynesian[j],hexList[i],hexList[i]);
				}
				if ( i > hexList.length)
				{
					i = 0;
				}
				i++;
				setTimeout("colorChangePoly()",100);
			}

			function colorChangeAsia()
			{
				for (var j = 0; j < xAsian.length; j++)
				{
					point(xAsian[j],yAsian[j],hexList[i],hexList[i]);
				}
				if ( i > hexList.length)
				{
					i = 0;
				}
				i++;
				setTimeout("colorChangeAsia()",100);
			}

				function colorChangeEuro()
				{
					for (var j = 0; j < xEuropean.length; j++)
					{
						point(xEuropean[j],yEuropean[j],hexList[i],hexList[i]);
					}
					if ( i > hexList.length)
					{
						i = 0;
					}
					i++;
					setTimeout("colorChangeEuro()",100);
				}

			function point(x,y,w,z)
			{
				ctx.fillStyle = "#"+ w + z + w + z + w + z +"";
				ctx.beginPath();
				ctx.shadowBlur=1.5;
				ctx.shadowColor="#"+ w + z + w + z + w + z +"";
				ctx.fillRect(x,y,1.5,1.5);
				ctx.stroke();
				ctx.fill();
			}

			function X(n) { return (n);}
			function Y(n) { return (myCanvas.height-n);}

			function curveColor(x1,y1,x2,y2)
			{
				if ((y1 > y2) && (x1 < x2))
				{
					return "red";
				}
				if ((y1 < y2) && (x1 < x2))
				{
					return "blue";
				}
				if ((y1 > y2) && (x1 > x2))
				{
					return "green";
				}
				if ((y1 < y2) && (x1 > x2))
				{
					return "orange";
				}
				if ((y1 == y2) && (x1 > x2))
				{
					return "purple";
				}
				if ((y1 == y2) && (x1 < x2))
				{
					return "cyan";
				}
			}

			function Radcoef(n,x1,y1,x2,y2)
			{
				if ((y1 > y2) && (x1 < x2))
				{
					return ((n-1)*Math.PI);
				}
				if ((y1 < y2) && (x1 < x2))
				{
					return ((n)*Math.PI);
				}
				if ((y1 > y2) && (x1 > x2))
				{
					return ((n)*Math.PI);
				}
				if ((y1 < y2) && (x1 > x2))
				{
					return ((n-1)*Math.PI);
				}
				if ((y1 == y2) && (x1 > x2))
				{
					return ((n)*Math.PI);
				}
				if ((y1 == y2) && (x1 < x2))
				{
					return ((n-1)*Math.PI);
				}
			}

			function lineDist(x1,y1,x2,y2) { return Math.sqrt(Math.pow((x1 - x2),2) + Math.pow((y1 - y2),2));}
			function midX(x1,y1,x2,y2) { return ((x1 + x2)/2);}
			function midY(x1,y1,x2,y2) { return ((y1 + y2)/2);}
			function highSchoolTrig(x1,y1,x2,y2) { return Math.acos((Math.pow(r,2) + Math.pow(r,2) - Math.pow(lineDist(x1,y1,x2,y2),2))/(2*r*r));}
			function triangleMidLineDist(x1,y1,x2,y2) { return (r*Math.cos(highSchoolTrig(x1,y1,x2,y2)/2));}

			function Slope(x1,y1,x2,y2)
			{
				if ((y1 == y2) && (x1 != x2))
				{
					return "No";
				}
				else
				{
					return ((y2 - y1)/(x2 - x1));
				}
			}

			function NrSlope(x1,y1,x2,y2)
			{
				if ((y1 == y2) && (x1 != x2))
				{
					return "No";
				}
				else
				{
					return (-1/Slope(x1,y1,x2,y2));
				}
			}

			function SQRTtan(x1,y1,x2,y2)
			{
				if ((y1 == y2) && (x1 != x2))
				{
					return "No";
				}
				else
				{
					return (Math.sqrt(1 + Math.pow(NrSlope(x1,y1,x2,y2),2)));
				}
			}

			function originX(x1,y1,x2,y2)
			{
				if ((y1 == y2) && (x1 > x2))
				{
					return (midX(x1,y1,x2,y2) + 0);
				}
				if ((y1 == y2) && (x1 < x2))
				{
					return (midX(x1,y1,x2,y2) + 0);
				}
				else
				{
					return (midX(x1,y1,x2,y2) + (triangleMidLineDist(x1,y1,x2,y2)/SQRTtan(x1,y1,x2,y2)));
				}
			}

			function originY(x1,y1,x2,y2)
			{
				if ((y1 == y2) && (x1 > x2))
				{
					return (midY(x1,y1,x2,y2) - triangleMidLineDist(x1,y1,x2,y2));
				}
				if ((y1 == y2) && (x1 < x2))
				{
					return (midY(x1,y1,x2,y2) + triangleMidLineDist(x1,y1,x2,y2));
				}
				else
				{
					return (midY(x1,y1,x2,y2) + ((triangleMidLineDist(x1,y1,x2,y2)*NrSlope(x1,y1,x2,y2))/SQRTtan(x1,y1,x2,y2)));
				}
			}

			function NewPointX(x1,y1,x2,y2)
			{
				if ((y1 > y2) && (x1 < x2))
				{
					return (originX(x1,y1,x2,y2) - r);
				}
				if ((y1 < y2) && (x1 < x2))
				{
					return (originX(x1,y1,x2,y2) + r);
				}
				if ((y1 > y2) && (x1 > x2))
				{
					return (originX(x1,y1,x2,y2) + r);
				}
				if ((y1 < y2) && (x1 > x2))
				{
					return (originX(x1,y1,x2,y2) - r);
				}
				if ((y1 == y2) && (x1 > x2))
				{
					return (originX(x1,y1,x2,y2) - r);
				}
				if ((y1 == y2) && (x1 < x2))
				{
					return (originX(x1,y1,x2,y2) - r);
				}
			}

			function trueFalse(x1,y1,x2,y2)
			{
				if ((y1 > y2) && (x1 < x2))
				{
					return false;
				}
				if ((y1 < y2) && (x1 < x2))
				{
					return true;
				}
				if ((y1 > y2) && (x1 > x2))
				{
					return false;
				}
				if ((y1 < y2) && (x1 > x2))
				{
					return true;
				}
				if ((y1 == y2) && (x1 > x2))
				{
					return true;
				}
				if ((y1 == y2) && (x1 < x2))
				{
					return false;
				}
			}

			function NewPointY(x1,y1,x2,y2){return (originY(x1,y1,x2,y2));}
			function firstRad(x1,y1,x2,y2) { return highSchoolTrig(NewPointX(x1,y1,x2,y2),NewPointY(x1,y1,x2,y2),x1,y1);}
			function secondRad(x1,y1,x2,y2) { return highSchoolTrig(NewPointX(x1,y1,x2,y2),NewPointY(x1,y1,x2,y2),x2,y2);}

			function groupDrawLine(firstLocation,secondLocation,index)
			{
				if (index == 0)
				{
					var Xlist = [(185+xOffset),(120+xOffset),(180+xOffset),(105+xOffset),(230+xOffset),(310+xOffset),(555+xOffset),(365+xOffset),(540+xOffset),(505+xOffset),(275+xOffset),(60+xOffset),(360+xOffset),(140+xOffset)];
					var Ylist = [(130+yOffset),(120+yOffset),(150+yOffset),(150+yOffset),(250+yOffset),(120+yOffset),(295+yOffset),(110+yOffset),(155+yOffset),(145+yOffset),(85+yOffset),(90+yOffset),(170+yOffset),(185+yOffset)];
					var ii;
					var jj;

					if (firstLocation == "Ottowa")
						ii = 0;
					else if (firstLocation == "Calgary")
						ii = 1;
					else if (firstLocation == "Washington")
						ii = 2;
					else if (firstLocation == "San Francisco")
						ii = 3;
					else if (firstLocation == "Brazilia")
						ii = 4;
					else if (firstLocation == "London")
						ii = 5;
					else if (firstLocation == "Canberra")
						ii = 6;
					else if (firstLocation == "Moscow")
						ii = 7;
					else if (firstLocation == "Tokyo")
						ii = 8;
					else if (firstLocation == "Beijing")
						ii = 9;
					else if (firstLocation == "Reykjavik")
						ii = 10;
					else if (firstLocation == "Anchorage")
						ii = 11;
					else if (firstLocation == "Cairo")
						ii = 12;
					else
						ii = 13;

					if (secondLocation == "Ottowa")
						jj = 0;
					else if (secondLocation == "Calgary")
						jj = 1;
					else if (secondLocation == "Washington")
						jj = 2;
					else if (secondLocation == "San Francisco")
						jj = 3;
					else if (secondLocation == "Brazilia")
						jj = 4;
					else if (secondLocation == "London")
						jj = 5;
					else if (secondLocation == "Canberra")
						jj = 6;
					else if (secondLocation == "Moscow")
						jj = 7;
					else if (secondLocation == "Tokyo")
						jj = 8;
					else if (secondLocation == "Beijing")
						jj = 9;
					else if (secondLocation == "Reykjavik")
						jj = 10;
					else if (secondLocation == "Anchorage")
						jj = 11;
					else if (secondLocation == "Cairo")
						jj = 12;
					else
						jj = 13;

					var xx1 = Xlist[ii];
					var yy1 = Ylist[ii];
					var xx2 = Xlist[jj];
					var yy2 = Ylist[jj];

				}
				if (index == 1)
				{
					var XlistCari = [(120+xOffset),(173+xOffset),(180+xOffset),(190+xOffset)];
					var YlistCari = [(120+yOffset),(183+yOffset),(186+yOffset),(190+yOffset)];
					var ii;
					var jj;

					if (firstLocation == "Calgary")
						ii = 0;
					else if (firstLocation == "Cuba")
						ii = 1;
					else if (firstLocation == "Dominican Republic")
						ii = 2;
					else if (firstLocation == "Puerto Rico")
						ii = 3;

					if (secondLocation == "Calgary")
						jj = 0;
					else if (secondLocation == "Cuba")
						jj = 1;
					else if (secondLocation == "Dominican Republic")
						jj = 2;
					else if (secondLocation == "Puerto Rico")
						jj = 3;

					var xx1 = XlistCari[ii];
					var yy1 = YlistCari[ii];
					var xx2 = XlistCari[jj];
					var yy2 = YlistCari[jj];

				}
				if (index == 2)
				{
					var XlistPoly = [(120+xOffset),(50+xOffset),(621+xOffset),(592+xOffset)];
					var YlistPoly = [(120+yOffset),(190+yOffset),(252+yOffset),(318+yOffset)];
					var ii;
					var jj;

					if (firstLocation == "Calgary")
						ii = 0;
					else if (firstLocation == "Hawaii")
						ii = 1;
					else if (firstLocation == "Samoa")
						ii = 2;
					else if (firstLocation == "New Zealand")
						ii = 3;

					if (secondLocation == "Calgary")
						jj = 0;
					else if (secondLocation == "Hawaii")
						jj = 1;
					else if (secondLocation == "Samoa")
						jj = 2;
					else if (secondLocation == "New Zealand")
						jj = 3;

					var xx1 = XlistPoly[ii];
					var yy1 = YlistPoly[ii];
					var xx2 = XlistPoly[jj];
					var yy2 = YlistPoly[jj];

				}
				if (index == 3)
				{
					var XlistAsia = [(120+xOffset),(505+xOffset),(538+xOffset),(480+xOffset)];
					var YlistAsia = [(120+yOffset),(145+yOffset),(155+yOffset),(195+yOffset)];
					var ii;
					var jj;

					if (firstLocation == "Calgary")
						ii = 0;
					else if (firstLocation == "Beijing")
						ii = 1;
					else if (firstLocation == "Tokyo")
						ii = 2;
					else if (firstLocation == "Thailand")
						ii = 3;

					if (secondLocation == "Calgary")
						jj = 0;
					else if (secondLocation == "Beijing")
						jj = 1;
					else if (secondLocation == "Tokyo")
						jj = 2;
					else if (secondLocation == "Thailand")
						jj = 3;

					var xx1 = XlistAsia[ii];
					var yy1 = YlistAsia[ii];
					var xx2 = XlistAsia[jj];
					var yy2 = YlistAsia[jj];

				}
				if (index == 4)
				{
					var XlistEuro = [(120+xOffset),(330+xOffset),(312+xOffset),(305+xOffset)];
					var YlistEuro = [(120+yOffset),(140+yOffset),(124+yOffset),(117+yOffset)];
					var ii;
					var jj;

					if (firstLocation == "Calgary")
						ii = 0;
					else if (firstLocation == "Rome")
						ii = 1;
					else if (firstLocation == "Paris")
						ii = 2;
					else if (firstLocation == "London")
						ii = 3;

					if (secondLocation == "Calgary")
						jj = 0;
					else if (secondLocation == "Rome")
						jj = 1;
					else if (secondLocation == "Paris")
						jj = 2;
					else if (secondLocation == "London")
						jj = 3;

					var xx1 = XlistEuro[ii];
					var yy1 = YlistEuro[ii];
					var xx2 = XlistEuro[jj];
					var yy2 = YlistEuro[jj];

				}

				ctx.beginPath();
				ctx.strokeStyle = "#20B2AA";
				ctx.arc(originX(xx1,yy1,xx2,yy2),originY(xx1,yy1,xx2,yy2),r,Radcoef((firstRad(xx1,yy1,xx2,yy2)/Math.PI),xx1,yy1,xx2,yy2),Radcoef((secondRad(xx1,yy1,xx2,yy2)/Math.PI),xx1,yy1,xx2,yy2),trueFalse(xx1,yy1,xx2,yy2));
				ctx.stroke();

			}

			function masterTravelExperts(vacationType)
			{
				if (vacationType == 0)
				{
					colorChange();
					lstOfPlaces = ["Ottowa","Calgary","Washington","San Francisco","Brazilia","London","Canberra","Moscow","Tokyo","Beijing","Reykjavik","Anchorage","Cairo"];
					for (var m = 0; m < lstOfPlaces.length; m++)
					{
						for (var n = 0; n < lstOfPlaces.length; n++)
						{
							groupDrawLine(lstOfPlaces[m],lstOfPlaces[n],0);
						}
					}
				}
				else if (vacationType == 1)
				{
					colorChangeCari();
					groupDrawLine("Calgary","Cuba",1);
					groupDrawLine("Calgary","Dominican Republic",1);
					groupDrawLine("Calgary","Puerto Rico",1);
				}
				else if (vacationType == 2)
				{
					colorChangePoly();
					groupDrawLine("Calgary","Hawaii",2);
					groupDrawLine("Calgary","Samoa",2);
					groupDrawLine("Calgary","New Zealand",2);
				}
				else if (vacationType == 3)
				{
					colorChangeAsia();
					groupDrawLine("Calgary","Beijing",3);
					groupDrawLine("Calgary","Tokyo",3);
					groupDrawLine("Calgary","Thailand",3);
				}
				else
				{
					colorChangeEuro();
					groupDrawLine("Calgary","Rome",4);
					groupDrawLine("Calgary","Paris",4);
					groupDrawLine("Calgary","London",4);
				}
			}

			function rotateThroughMaster()
			{
				setTimeout(function(){ masterTravelExperts(1) }, 2000);
				setTimeout(function(){ masterTravelExperts(2) }, 4000);
				setTimeout(function(){ masterTravelExperts(3) }, 6000);
				setTimeout(function(){ masterTravelExperts(4) }, 8000);
			}
			</script>

		</div>
	</div>
</div>
