<!--
    Created by Matt Jennings
-->

<!--Navbar-->
<nav class="navbar navbar-dark navbar-fixed-top scrolling-navbar teal">

    <!-- Collapse button-->
    <div class="navbar-toggler hidden-sm-up">
        <div class="hamburger-container">
            <div id="nav-icon4" data-toggle="collapse" data-target="#collapseEx">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>

    <div class="container">

        <!--Collapse content-->

        <div class="collapse navbar-toggleable-xs" id="collapseEx">
            <!--Navbar Brand-->
            <div class="navbar-logo"> <img src="img/logo.png" /></div>

                <!--Links-->
                <ul class="nav navbar-nav">
                    <?php
                        $pageFileName = basename($_SERVER['PHP_SELF']);

                        if ($pageFileName == "index.php") {
                            print("
                                    <li class='nav-item'>
                                        <a class='nav-link' onClick='scrollToId(\"#home\")'>Home</a>
                                    </li>
                                    <li class='nav-item'>
                                        <a class='nav-link' onClick='scrollToId(\"#vacationpackages\")'>Vacation packages</a>
                                    </li>
                                    <li class='nav-item'>
                                        <li class='nav-item btn-group'>
                                        <a class='nav-link dropdown-toggle' id='aboutUsDropDown' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>About Us</a>
                                        <div class='dropdown-menu' aria-labelledby='aboutUsDropDown'>
                                            <a class='dropdown-item' onClick='scrollToId(\"#about-us-map\")'>What We Do</a>
                                            <a class='dropdown-item' onClick='scrollToId(\"#Works\")'>How It Works</a>
                                            <a class='dropdown-item' onClick='scrollToId(\"#Money\")'>How We Make Money</a>
                                            <a class='dropdown-item' onClick='scrollToId(\"#Travel-tips\")'>Travel Tips</a>
                                            <a class='dropdown-item' onClick='scrollToId(\"#Team\")'>Our Team</a>
                                        </div>
                                        </li>
                                </li>
                            ");
                        } else {
                            print("
                                <li class='nav-item'>
                                    <a class='nav-link' href='index.php'>Home </a>
                                </li>
                                <li class='nav-item'>
                                    <a class='nav-link' href='index.php#vacationpackages')'>Vacation packages</a>
                                </li>
                            ");
                        }
                    ?>
                    <div class="right-aligned-nav-item">
                        <li class="nav-item">
                            <a class="nav-link waves-effect waves-light" data-toggle="modal" data-target="#modal-contact"><i class="fa fa-envelope"></i>  Contact us</a>
                        </li>
                        <?php
                            // Switch login button to profile if user is logged in                            
                            if (!isset($_SESSION['CustomerId'])) {
                                print("
                                    <li class='nav-item'>
                                        <a class='nav-link waves-effect waves-light' data-toggle='modal' data-target='#modal-login'><i class='fa fa-user'></i>  Login</a>
                                    </li>
                                ");
                            } else {
                                print("
                                    <li class='nav-item'>
                                        <a href='php/logout.php'class='nav-link waves-effect waves-light'><i class='fa fa-sign-out' aria-hidden='true'></i>  Logout</a>
                                    </li>
                                ");
                            }
                        ?>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</nav>
