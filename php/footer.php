<!--Authored by parneet-->
<!-- Created by Parneet Kaur -->

<div class="container-fluid">
	<div class="row" id="footer">

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="locations">
			<h2>FIND US</h2>
			<p>We are all around the world.</br>Contact us and start improving your travel.</p>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="socialMedia">
			<div id="socialMediaIcons" class="text-center center-block">
				<i id="social-fb" class="fa fa-facebook-square fa-3x social" ></i>
				<i id="social-tw" class="fa fa-twitter-square fa-3x social"></i>
				<i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i>
				<i id="social-em" class="fa fa-envelope-square fa-3x social"></i>
			</div>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="careers">
			<h4>CAREERS</h4>
			<p>Explore our current career opportunities!</p>

			<a href="php/careers.php" id="careersBtn" onclick="openWin()"role="button" value="Join the team" class="btn">Join the team.</a>
		</div>


		<div id="copyrightStatement" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
			<p>COPYRIGHT &copy;2016 ALL RIGHTS RESERVED</p>
		</div>
	</div>
</div>
