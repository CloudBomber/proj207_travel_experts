function validate()
{

	//alert("Your validate function is working!");
	alert("Your validate functino is working");

	var stat = true;

	var field1 = document.forms.getElementByTagName("CustFirstName");
	if (field1.value == "")
	{
		alert("Must enter your first name");
		field1.style.backgroundColor = "cyan";
		stat = false;
	}

	var field2 = document.forms[0].CustLastName;
	if (field2.value == "")
	{
		alert("Must enter your last name");
		field2.style.backgroundColor = "cyan";
		stat = false;
	}

	var field3 = document.forms[0].CustAddress;
	if (field3.value == "")
	{
		alert("Must enter your address");
		field3.style.backgroundColor = "cyan";
		stat = false;
	}

	var field4 = document.forms[0].CustCity;
	if (field4.value == "")
	{
		alert("Must enter your city");
		field4.style.backgroundColor = "cyan";
		stat = false;
	}

	var field5 = document.forms[0].CustProv;
	if (field5.value == "")
	{
		alert("Must enter your province/state");
		field5.style.backgroundColor = "cyan";
		stat = false;
	}

	var field6 = document.forms[0].CustCountry;
	if (field6.value == "")
	{
		alert("Must enter your country");
		field6.style.backgroundColor = "cyan";
		stat = false;
	}

	var field7 = document.forms[0].CustHomePhone;
	if (field7.value == "")
	{
		alert("Must enter your home phone #");
		field7.style.backgroundColor = "cyan";
		stat = false;
	}

	var field8 = document.forms[0].CustPassword;
	if (field8.value == "")
	{
		alert("Must enter your password");
		field8.style.backgroundColor = "cyan";
		stat = false;
	}

	var regexpr1 = /\b\w+\.[a-z0-9_]+@[a-z]+\.ca\b/gi;
	var field6 = document.forms[0].CustEmail;
	if ((regexpr1.test(field6.value) != true))
	{
		alert("Must be a valid Email address");
		field6.style.backgroundColor = "cyan";
		stat = false;
	}

	var regexpr2 = /^[A-Z]\d[A-Z] ?\d[A-Z]\d$/;
	var field7 = document.forms[0].CustPostal;
	if ((regexpr2.test(field7.value) != true))
	{
		alert("Must be a valid postal code");
		field7.style.backgroundColor = "cyan";
		stat = false;
	}

	return stat;
}
