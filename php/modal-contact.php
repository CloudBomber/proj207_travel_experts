<!-- Kasi Emmanuel & Matt Jennings -->
<?php
  include ("php/variables.php");
 ?>
<!-- Modal Contact -->
<div class="modal fade modal-ext" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="contact-form-header" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h4 class="modal-title" id="contact-form-header">We'd love to hear from you</h4>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" id="showCalgaryBtn" class="btn btn-info agency-button active" onClick="showAgency(this)">Calgary Branch</button>
                    </div>
                    <div class="col-md-6">
                        <button type="button" id="showOkotoksBtn" class="btn btn-info agency-button" onClick="showAgency(this)">Okotoks Branch</button>
                    </div>
                </div>
                  <?php
                     require('php/classes/agent.class.php');
                     require('php/classes/agency.class.php');
                     $dbh =mysqli_connect($host, $user, $password, $database);

                     //put connection checking here
                     if (!$dbh)
                     {
                       print(mysqli_connect_error());
                     }

                     $sql = "SELECT agents.AgencyId, agents.AgtFirstName, agents.AgtLastName, agents.AgtPosition, agencies.AgncyAddress, agencies.AgncyCity, agencies.AgncyProv, agencies.AgncyPostal, agencies.AgncyCountry, agents.AgtBusPhone, agents.AgtEmail\n"
                         . "FROM agencies\n"
                         . "INNER JOIN agents\n"
                         . "ON agencies.AgencyId=agents.AgencyId\n"
                         . "\n"
                         . "";

                     $result = mysqli_query($dbh, $sql);

                     //check if result is there
                     if (!$result)
                     {
                       mysqli_error($dbh);
                     }

                     // Since we're in a loop, we only want to print the agency information once
                     $agenciesPrinted = array(false, false);

                     while ($row = mysqli_fetch_assoc($result)) {
                         $agent = new Agent($row);
                         $agency = new Agency($row);

                         $agencyClass = "agency-calgary";
                         $agencyId = $agency->getId() - 1;

                         if ($agency->getId() == 2) {
                             $agencyClass = "agency-okotoks";
                         }

                         if ($agenciesPrinted[$agencyId] == false) {
                            print("
                                <div class='$agencyClass agency-description'>
                                    {$agency->getCity()}, {$agency->getProvince()} {$agency->getCountry()} <br> </strong>{$agency->getAddress()} {$agency->getPostalCode()}
                                </div>
                            ");
                            $agenciesPrinted[$agencyId] = true;
                         }

                         print("
                            <div class='$agencyClass agent-description col-md-6 col-xs-12'>
                                <div class='agent-name'>
                                    {$agent->getFirstName()} {$agent->getLastName()}
                                </div>
                                <div class='agent-position'>
                                    {$agent->getPosition()}
                                </div>
                                <div class='agent-phone'>
                                    1 {$agent->getBusPhone()}
                                </div>
                                <div class='agent-email'>
                                    {$agent->getEmail()}
                                </div>
                            </div>

                         ");

                     }
                     mysqli_close($dbh);
                   ?>
                <div class="contact-message-us">
                    <p>If you would prefer to give send us a message, you can do so below</p>
                </div>
                <form id="contact-us-form">
                <div class="md-form">
                    <i class="fa fa-user prefix"></i>
                    <input type="text" id="contact-form-name" class="form-control" required="required">
                    <label for="contact-form-name">Your name</label>
                </div>

                <div class="md-form">
                    <i class="fa fa-envelope prefix"></i>
                    <input id="contact-form-email" class="form-control"  type="email" required="required"></input>
                    <label for="contact-form-email">Your email</label>
                </div>

                <div class="md-form">
                    <i class="fa fa-tag prefix"></i>
                    <input type="text" id="contact-form-subject" class="form-control" required="required">
                    <label for="contact-form-subject">Subject</label>
                </div>

                <div class="md-form">
                    <i class="fa fa-pencil prefix"></i>
                    <textarea type="text" id="contact-form-message" class="md-textarea" required="required"></textarea>
                    <label for="contact-form-message">Message</label>
                </div>

                <div class="text-xs-center">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
                </form>
            </div>
            <!--Footer-->
            <!-- Kasi Emmanuel -->
            <div class="modal-footer">
                <!-- /.Live preview -->
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
