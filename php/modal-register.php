<!--      
     Structured by Justin Pilon
     Design touchups done by Matt Jennings
     RegEX coded by Kasi Emmanuel  
-->
<!-- Modal Register -->
<form action="php/functionRegistration.php" method="post">
<div class="modal fade modal-ext" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="signup-form-header" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h3 id="signup-form-header"><i class="fa fa-user"></i> Sign Up</h3>
            </div>

            <div class="modal-body">
              <div class="col-md-6 col-sm-12">
                <label for="customerFirstName">First name:</label>
                <input name="CustFirstName" id="customerFirstName" class="form-control" type="text" required="required"></input>
              </div>

              <div class="col-md-6 col-sm-12">
                <label for="customerLastName">Last name:</label>
                <input name="CustLastName" id="customerLastName" class="form-control" type="text" required="required"></input>
              </div>

              <!--Inserted by Kasi Emmanuel-->
              <div class="col-xs-3">
                <label for="customerCity">City:</label>
                <input name="CustCity" id="customerCity" class="form-control" type="text" required="required"></input>
              </div>
              <div class="col-xs-3">
                <label for="customerProv">Province:</label>
                <!-- <input name="CustProv" id="customerProv" class="form-control" type="text" required="required"></input> -->
                <div class="material-select">
                  <select name="CustProv" style="margin: 0;">
                    <option value="AB">AB</option>
                    <option value="BC">BC</option>
                    <option value="SK">SK</option>
                    <option value="MB">MB</option>
                    <option value="ON">ON</option>
                  </select>
                </div>
              </div>
              <div class="col-xs-3">
                <label for="customerPostal">Postal:</label>
                <input name="CustPostal" id="customerPostal" class="form-control" type="text" required="required"></input>
              </div>

              <div class="col-xs-3">
                <label for="customerCountry">Country:</label>
                <input name="CustCountry" id="customerCountry" class="form-control" type="text" required="required"></input>
              </div>

              <div class="col-xs-12">
                <label for="customerAddress">Address:</label>
                <input name="CustAddress" id="customerAddress" class="form-control" type="text" required="required"></input>
              </div>
              <!--End Inserted by Kasi Emmanuel-->

              <div class="col-xs-6">
                <label for="customerHomePhone">Home Phone (xxx-xxx-xxxx):</label>
                <input name="CustHomePhone" id="customerHomePhone" class="form-control" type="tel" pattern="^\d{3}-\d{3}-\d{4}$"  required="required"></input>
              </div>

              <div class="col-xs-6">
                <label for="customerBusPhone">Business Phone (xxx-xxx-xxxx):</label>
                <input name="CustBusPhone" id="customerBusPhone" class="form-control" type="tel" pattern="^\d{3}-\d{3}-\d{4}$"></input>
              </div>

              <div class="col-xs-12">
                <label for="customerEmail">Email:</label>
                <input name="CustEmail" id="customerEmail" class="form-control" type="email" required="required"></input>
              </div>

              <div class="col-xs-12">
                <label for="customerPassword">Password:</label>
                <input name="CustPassword" id="CustPassword" class="form-control" type="password" required="required"></input>
              </div>

                <div class="text-xs-center">
                  <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                </div>
            </div>

            <!--Footer-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
</form>
