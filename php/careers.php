<!-- Created by Kasi Emmanuel -->


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Career Opportunities at Travel Experts</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design Bootstrap -->
        <link href="css/mdb.min.css" rel="stylesheet">

        <!-- Custom styles -->
        <link href="css/style.css" rel="stylesheet">

        <link rel="stylesheet" href="css/footer.css">
        <style>
        body
        {
          height: auto;
          font-size: 2em;
          color: #292929;
          background-position: center;
          background-image: url("../img/carousel/temple.jpg");
        }
        </style>
        <script>setTimeout(function(){window.location.href='../index.php'},3000);</script>
    </head>

    <body>


          <div class="container-fluid" style="margin-top:200px;">
            <div class="jumbotron" id="careerspage">
              <h1 class="header" align="center">This page is currently <br/>under Construction.</h1>
            </div>
          </div>


        <!-- SCRIPTS -->

        <!-- JQuery -->
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>


        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="js/tether.min.js"></script>

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="js/mdb.min.js"></script>

        <script type="text/javascript" src="js/main.js"></script>
    </body>

</html>
