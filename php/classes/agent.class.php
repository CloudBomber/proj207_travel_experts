<?php
// Created by Matt Jennings - Nov 7th 2016
// This class is used to store agent data in the same form as the database table
class Agent {
    private $id;
    private $firstName;
    private $middleInitial;
    private $lastName;
    private $busPhone;
    private $email;
    private $position;
    private $userId;
    private $password;

    // Takes an associative array with values equal to the table column names
    function Agent($data) {
        // Defaults
        $this->id = "-1";
        $this->firstName = "NOT_SET";
        $this->middleInitial = "NOT_SET";
        $this->lastName = "NOT_SET";
        $this->busPhone = "NOT_SET";
        $this->email = "NOT_SET";
        $this->position = "NOT_SET";
        $this->userId = "NOT_SET";
        $this->password = "NOT_SET";

        if (isset($data['AgentId'])) {
            $this->id = $data['AgentId'];
        }
     
        if (isset($data['AgtFirstName'])) {
            $this->firstName = $data['AgtFirstName'];
        }

        if (isset($data['AgtMiddleInitial'])) {
            $this->middleInitial = $data['AgtMiddleInitial'];
        }

        if (isset($data['AgtLastName'])) {
            $this->lastName = $data['AgtLastName'];
        }

        if (isset($data['AgtBusPhone'])) {
            $this->busPhone = $data['AgtBusPhone'];
        }

        if (isset($data['AgtEmail'])) {
            $this->email = $data['AgtEmail'];
        }

        if (isset($data['AgtPosition'])) {
            $this->position = $data['AgtPosition'];
        }

        if (isset($data['AgtUserId'])) {
            $this->userId = $data['AgtUserId'];
        }        

        if (isset($data['AgtPassword'])) {
            $this->password = $data['AgtPassword'];
        }                
    }

    // Getters and setters
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getFirstName() {     
        return $this->firstName;
    }

    public function setFirstName($name) {
        $this->firstName = $name;
    }

    public function getMiddleInitial() {
        return $this->middleInitial;        
    }

    public function setMiddleInitial($initial) {
        $this->middleInitial = $initial;
    }

    public function getLastName() {
        return $this->lastName;
    }    

    public function setLastName($name) {
        $this->lastName = $name;
    }
    
    public function getBusPhone() {
        return $this->busPhone;
    }

    public function setBusPhone($phone) {
        $this->busPhone = $phone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getPosition() {
        return $this->position;
    }    

    public function setPosition($position) {
        $this->position = $position;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId($uid) {
        $this->userId = $uid;
    }    
    
    public function getPassword() {
        return $this->password;
    }    
    
    public function setPassword($pwd) {
        $this->password = $pwd;
    }    

    public function toString() {
        return  "ID: " . $this->getId() .
                ", First Name: " . $this->getFirstName() .
                ", Initial: " . $this->getMiddleInitial() .
                ", Last Name: " . $this->getLastName() .
                ", Email: " . $this->getEmail() .
                ", Bus Phone: " . $this->getBusPhone() . 
                ", Position: " . $this->getPosition() .
                ", User ID: " . $this->getUserId();
    }     

    // Returns names of columns needed for SQL query. ID is optional 
    // $returnId is a boolean that, if true, includes the AgentId field    
    public function sqlColumns($returnId) {
        $string = "";

        if ($returnId) 
            $string = "AgentId, ";

        $string .= "AgtFirstName, AgtMiddleInitial, AgtLastName, AgtEmail, AgtBusPhone, AgtPosition";
        return $string;
    }

    // Returns names of column values for SQL query.
    // $returnId is a boolean that, if true, includes the AgentId field
    public function sqlValues($returnId) {
        $string = "";

        if ($returnId)
            $string = "'" + $this.getId() . "', ";

        $string .=  "'".$this->getFirstName() . "'" .
                    ", '" . $this->getMiddleInitial() . "'" .
                    ", '" . $this->getLastName() . "'" .
                    ", '" . $this->getEmail() . "'" .
                    ", '" . $this->getBusPhone() . "'" .
                    ", '" . $this->getPosition() . "'";
        return $string;                     
    }
}
?>