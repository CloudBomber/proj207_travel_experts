 <?php
// Created by Matt Jennings - Nov 7th 2016
// This class is used to store product data in the same form as the database table
class Product {
    private $id;
    private $name;

    function Product($data) {

        $this->id = "-1";
        $this->name = "NOT_SET";

        if (isset($data['ProductId'])) {
            $this->id = $data['ProductId'];
        }

        if (isset($data['ProdName'])) {
            $this->name = $data['ProdName'];
        }        
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }
}

 ?>