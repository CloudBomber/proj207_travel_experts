<?php
// Created by Matt Jennings - Nov 7th 2016
// This class is used to store customer data in the same form as the database table
class Customer {

    private $id;
    private $firstName;
    private $lastName;
    private $address;
    private $city;
    private $province;
    private $postalCode;
    private $country;
    private $homePhone;
    private $busPhone;
    private $email;
    private $agentId;
    private $password;

    function Customer($data) {
        // Defaults
        $this->id = "-1";
        $this->firstName = "NOT_SET";
        $this->lastName = "NOT_SET";
        $this->address = "NOT_SET";
        $this->city = "NOT_SET";
        $this->province = "NOT_SET";
        $this->postalCode = "NOT_SET";
        $this->country = "NOT_SET";
        $this->homePhone = "NOT_SET";
        $this->busPhone = "NOT_SET";
        $this->email = "NOT_SET";
        $this->password = "NOT_SET";
        $this->agentId = "-1";

        if (isset($data['CustomerId'])) {
            $this->id = $data['CustomerId'];
        }

        if (isset($data['CustFirstName'])) {
            $this->firstName = $data['CustFirstName'];
        }

        if (isset($data['CustLastName'])) {
            $this->lastName = $data['CustLastName'];
        }

        if (isset($data['CustAddress'])) {
            $this->address = $data['CustAddress'];
        }

        if (isset($data['CustCity'])) {
            $this->city = $data['CustCity'];
        }

        if (isset($data['CustProv'])) {
            $this->province = $data['CustProv'];
        }

        if (isset($data['CustPostal'])) {
            $this->postalCode = $data['CustPostal'];
        }

        if (isset($data['CustCountry'])) {
            $this->country = $data['CustCountry'];
        }

        if (isset($data['CustHomePhone'])) {
            $this->homePhone = $data['CustHomePhone'];
        }

        if (isset($data['CustBusPhone'])) {
            $this->busPhone = $data['CustBusPhone'];
        }

        if (isset($data['CustEmail'])) {
            $this->email = $data['CustEmail'];
        }

        if (isset($data['AgentId'])) {
            $this->agentId = $data['AgentId'];
        }
        if (isset($data['CustPassword'])) {
            $this->password = $data['CustPassword'];
        }
    }

    // Getters and setters
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($name) {
        $this->firstName = $name;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($name) {
        $this->lastName = $name;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getProvince() {
        return $this->province;
    }

    public function setProvince($province) {
        $this->province = $province;
    }

    public function getPostalCode() {
        return $this->postalCode;
    }

    public function setPostalCode($postalCode) {
        $this->postalCode = $postalCode;
    }

    public function getCountry() {
        return $this->country;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function getHomePhone() {
        return $this->homePhone;
    }

    public function setHomePhone($phone) {
        $this->homePhone = $phone;
    }

    public function getBusPhone() {
        return $this->busPhone;
    }

    public function setBusPhone($phone) {
        $this->busPhone = $phone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getAgentId() {
        return $this->agentId;
    }

    public function setAgentId($id) {
        $this->agentId = $id;
    }

    public function getCustPassword() {
        return $this->CustPassword;
    }

    public function setCustPassword($password)
    {
        $this->CustPassword = bcrypt($password);

    }



    // Returns names of columns needed for SQL query. ID is optional
    // $returnId is a boolean that, if true, includes the CustomerId field  
    public function sqlColumns($returnId) {
        $string = "";

        if ($returnId) {
            $string = "CustomerId, ";
        }

        $string .= "CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustCountry, CustHomePhone, CustBusPhone, CustEmail, AgentId, CustPassword";
        return $string;
    }

    // Returns names of column values for SQL query. ID is optional
    // $returnId is a boolean that, if true, includes the CustomerId field  
    public function sqlValues($returnId) {
        $string = "";

        if ($returnId) {
            $string = "'" + $this->getId() . "', ";
        }
        $string .=  "'".$this->getFirstName() . "'" .
                    ", '" . $this->getLastName() . "'" .
                    ", '" . $this->getAddress() . "'" .
                    ", '" . $this->getCity() . "'" .
                    ", '" . $this->getProvince() . "'" .
                    ", '" . $this->getPostalCode() . "'" .
                    ", '" . $this->getCountry() . "'" .
                    ", '" . $this->getHomePhone() . "'" .
                    ", '" . $this->getEmail() . "'" .
                    ", '" . $this->getAgentId() . "'".
                    ", '" . $this->getCustPassword() . "'";
        return $string;
    }
}
?>
