<?php
// Created by Matt Jennings - Nov 7th 2016
// This class is used to store booking data in the same form as the database table
class Booking {
    private $id;
    private $date;
    private $bookingNumber; // could have just called this "number", but given we have an "id" property it might be confusing
    private $travelerCount;
    private $customerId;
    private $tripTypeId;
    private $packageId;

    // Takes an associative array with values equal to the table column names
    function Booking($data) {
        // Defaults
        $this->id = "-1";
        $this->date = "NOT_SET";
        $this->bookingNumber = "NOT_SET";
        $this->travelerCount = "NOT_SET";
        $this->customerId = "NOT_SET";
        $this->tripTypeId = "NOT_SET";
        $this->packageId = "NOT_SET";

        if (isset($data['BookingId'])) {
            $this->id = $data['BookingId'];
        }
     
        if (isset($data['BookingDate'])) {
            $this->date = $data['BookingDate'];
        }

        if (isset($data['BookingNo'])) {
            $this->bookingNumber = $data['BookingNo'];
        }

        if (isset($data['TravelerCount'])) {
            $this->travelerCount = $data['TravelerCount'];
        }

        if (isset($data['CustomerId'])) {
            $this->customerId = $data['CustomerId'];
        }

        if (isset($data['TripTypeId'])) {
            $this->tripTypeId = $data['TripTypeId'];
        }

        if (isset($data['PackageId'])) {
            $this->packageId = $data['PackageId'];
        }            
    }

    // Getters and setters
    public function getId() {
        return $this->id;
    }

    public function getDate() {     
        return $this->date;
    }

    public function getBookingNumber() {
        return $this->bookingNumber;        
    }

    public function getTravelerCount() {
        return $this->travelerCount;
    }    

    public function getCustomerId() {
        return $this->customerId;
    }

    public function getTripTypeId() {
        return $this->tripTypeId;
    }

    public function getPackageId() {
        return $this->packageId;
    }       

    // Returns names of columns needed for SQL query.
    // $returnId is a boolean that, if true, includes the BookingId field      
    public function sqlColumns($returnId) {
        $string = "";

        if ($returnId) 
            $string = "BookingId, ";

        $string .= "BookingDate, BookingNo, TravelerCount, TripTypeId, CustomerId, PackageId";
        return $string;
    }
}
?>