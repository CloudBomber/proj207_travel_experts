 <?php
// Created by Matt Jennings - Nov 7th 2016
// This class is used to store data in the same form as the database table
class PackagesProductsSuppliers {
    private $id;
    private $productSupplierId;

    function PackagesProductsSuppliers($data) {

        $this->id = "-1";
        $this->productSupplierId = "-1";

        if (isset($data['PackageId'])) {
            $this->id = $data['PackageId'];
        }

        if (isset($data['ProductSupplierId'])) {
            $this->productSupplierId = $data['ProductSupplierId'];
        }        
    }

    function getId() {
        return $this->id;
    }

    function getProductSupplierId() {
        return $this->productSupplierId;
    }
}

 ?>