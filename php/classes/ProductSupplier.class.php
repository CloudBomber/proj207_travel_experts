 <?php
// Created by Matt Jennings - Nov 7th 2016
// This class may not be necessary to use
class ProductSupplier {
    private $id;
    private $productId;
    private $supplierId;

    function ProductSupplier($data) {

        $this->id = "-1";        
        $this->productId = "-1";
        $this->supplierId = "-1";

        if (isset($data['ProductSupplierId'])) {
            $this->id = $data['ProductSupplierId'];
        }

        // ProductId and SupplierId could cause issues if we're getting data from a joined query that includes the supplier and produc tables...
        // should look at renaming the columns in the database
        if (isset($data['ProductId'])) {
            $this->productId = $data['ProductId'];
        }        

        if (isset($data['SupplierId'])) {
            $this->supplierId = $data['SupplierId'];
        }        
    }

    function getId() {
        return $this->id;
    }

    function getProductId() {
        return $this->productId;
    }

    function getSupplierId() {
        return $this->supplierId;
    }
}

 ?>