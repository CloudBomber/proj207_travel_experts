 <?php
// Created by Matt Jennings - Nov 7th 2016
// This class is used to store supplier data in the same form as the database table
class Supplier {
    private $id;
    private $name;

    function Supplier($data) {

        $this->id = "-1";
        $this->name = "NOT_SET";

        if (isset($data['SupplierId'])) {
            $this->id = $data['SupplierId'];
        }

        if (isset($data['SupName'])) {
            $this->name = $data['SupName'];
        }        
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }
}

 ?>