<?php
// Created by Matt Jennings - Nov 7th 2016
// This class is used to store package data in the same form as the database table
class Package {
    private $id;
    private $name;
    private $startDate;
    private $endDate;
    private $description;
    private $basePrice;
    private $agencyCommission;

    // Takes an associative array with values equal to the table column names
    function Package($data) {
        // Defaults
        $this->id = "-1";
        $this->name = "NOT_SET";
        $this->startDate = "NOT_SET";
        $this->endDate = "NOT_SET";
        $this->description = "NOT_SET";
        $this->basePrice = "NOT_SET";
        $this->agencyCommission = "NOT_SET";

        if (isset($data['PackageId'])) {
            $this->id = $data['PackageId'];
        }

        if (isset($data['PkgName'])) {
            $this->name = $data['PkgName'];
        }

        if (isset($data['PkgStartDate'])) {
            $this->startDate = $data['PkgStartDate'];
        }

        if (isset($data['PkgEndDate'])) {
            $this->endDate = $data['PkgEndDate'];
        }

        if (isset($data['PkgDesc'])) {
            $this->description = $data['PkgDesc'];
        }

        if (isset($data['PkgBasePrice'])) {
            $this->basePrice = $data['PkgBasePrice'];
        }

        if (isset($data['PkgAgencyCommission'])) {
            $this->agencyCommission = $data['PkgAgencyCommission'];
        }
    }

    // Getters

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getBasePrice() {
        return floor($this->basePrice);
    }

    public function getAgencyCommission() {
        return $this->agencyCommission;
    }    
}
?>
