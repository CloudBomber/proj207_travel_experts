<?php
// Created by Matt Jennings - Nov 7th 2016
// This class is used to store agency data in the same form as the database table

class Agency {
    private $id;
    private $address;
    private $city;
    private $province;
    private $postalCode;
    private $country;
    private $phoneNumber;
    private $faxNumber;    

    // Takes an associative array with values equal to the table column names
    function Agency($data) {
        // Defaults
        $this->id = "-1";
        $this->address = "NOT_SET";
        $this->city = "NOT_SET";
        $this->province = "NOT_SET";
        $this->postalCode = "NOT_SET";
        $this->country = "NOT_SET";
        $this->phoneNumber = "NOT_SET";
        $this->faxNumber = "NOT_SET";

        if (isset($data["AgencyId"])) {
            $this->id = $data["AgencyId"];
        }
     
        if (isset($data["AgncyAddress"])) {
            $this->address = $data["AgncyAddress"];
        }

        if (isset($data["AgncyCity"])) {
            $this->city = $data["AgncyCity"];
        }

        if (isset($data["AgncyProv"])) {
            $this->province = $data["AgncyProv"];
        }

        if (isset($data["AgncyPostal"])) {
            $this->postalCode = $data["AgncyPostal"];
        }

        if (isset($data["AgncyCountry"])) {
            $this->country = $data["AgncyCountry"];
        }

        if (isset($data["AgncyPhone"])) {
            $this->phoneNumber = $data["AgncyPhone"];
        }

        if (isset($data["AgncyFax"])) {
            $this->faxNumber = $data["AgncyFax"];
        }        
    }

    // Getters
    // No setters, don"t need them

    public function getId() {
        return $this->id;
    }

    public function getAddress() {     
        return $this->address;
    }

    public function getCity() {
        return $this->city;        
    }

    public function getProvince() {
        return $this->province;
    }    
    
    public function getPostalCode() {
        return $this->postalCode;
    }

    public function getCountry() {
        return $this->country;
    }

    public function getPhoneNumber() {
        return $this->phoneNumber;
    }    

    public function getFaxNumber() {
        return $this->faxNumber;
    }   

    // Returns names of columns needed for SQL query. 
    // $returnId is a boolean that, if true, includes the AgencyId field
    public function sqlColumns($returnId) {
        $string = "";

        if ($returnId) 
            $string = "AgencyId, ";

        $string .= "AgncyAddress, AgncyCity, AgncyProv, AgncyPostal, AgncyCountry, AgncyPhone, AgncyFax";
        return $string;
    }
}
?>