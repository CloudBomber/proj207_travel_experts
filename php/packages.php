

<!-- Created by Kasi Emmanuel -->
<div class="container-fluid" id='vacationpackages'>

  <h1 class="header" id='vacationpackages' align="center">All Vacation Packages</h1>
</div>
<div id="package-gallery">
  <div class="container-fluid">
    <?php
        /* Date checking & css class insertion done by Matt Jennings */
        mysqli_data_seek($result, 0);

        print("<div class='row' id='package-gallery-container'>");

        $i = 1;
        while ($row = mysqli_fetch_assoc($result)) {
          $package = new Package($row);

          $startDate = new DateTime($package->getStartDate());
          $endDate = new DateTime($package->getEndDate());
          $startDateFormatted = date_format($startDate, 'M j Y');
          $endDateFormatted = date_format($endDate, 'M j Y');

          $startDatePassed = false;
          $endDatePassed = false;
          if (strtotime($startDateFormatted) < time()) {
              $startDatePassed = true;
          }

          if (strtotime($endDateFormatted) < time()) {
              $endDatePassed = true;
          }

          // This changes the offset based on the first or second column in the row. Keeps them centered with spacing.
          $offsetXLClass = $i % 2 == 1 ? "offset-xl-1" : "offset-xl-2";

          // Applies the expired class for overlay if the package has expired
          $expiredClass = $startDatePassed == true ? "package-expired" : "package-available";

          // Disables clicking if the package has expired
          $expiredLink = $startDatePassed == true ? "link-expired" : "link-available";
          print("
          <a class='package-link $expiredLink' ". (!$startDatePassed ? "onclick='goToPackageDetails({$package->getId()})" : "")."'>
              <div class='col-xl-4 $offsetXLClass col-sm-10 offset-sm-1 col-xs-10 offset-xs-1 z-depth-3 package-gallery-content $expiredClass'>
                <div class='img-packages-container'>
                  <div class='img-packages'></div>
                </div>
                <div align='center' class='Main-Content'>
                  <div class='gallery-info'>
                  <h5>{$package->getName()}</h5>
                  <h2>\${$package->getBasePrice()}</h2>
                  <p><span class='" . ($startDatePassed ? "date-passed" : "") . "'>$startDateFormatted</span> <br>to</br> <span class='" . ($endDatePassed ? "date-passed" : "") . "'>$endDateFormatted</span></p>
                  </div>
                  <div id='package-gallery-mobile-label'>
                  Click for more details
                  </div>
                </div>
              </div>
              </a>
            ");
            $i++;
        }
        print("</div>");
    ?>
  </div>
</div>
