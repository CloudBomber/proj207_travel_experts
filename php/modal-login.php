<!-- Created by Matt Jennings -->
<form method="get" action="php/classes/checkLogin.php">
<!-- Modal Login -->
<div class="modal fade modal-ext" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="login-form-header" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header">
                <h3 class="login-form-header"><i class="fa fa-user"></i> Login</h3>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="md-form">
                    <i class="fa fa-envelope prefix"></i>
                    <input type="text" id="form2" name="userId" class="form-control" required="required">
                    <label for="form2">Your email</label>
                </div>

                <div class="md-form">
                    <i class="fa fa-lock prefix"></i>
                    <input type="password" id="loginPassword" name="userPassword" class="form-control" required="required">
                    <label for="loginPassword">Your password</label>

                </div>

                <div class="text-xs-center">
                  <button class="btn btn-primary btn-lg" type="submit">Login</button>
                  <button class="btn btn-light-green btn-lg" data-dismiss="modal" onClick="openModalDelayed('#modal-register')">Sign Up</button>
                </div>

            </form>
            </div>

            <!--Footer-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>


</form>
