<?php
/* loginMessageAlert created by Parneet
   adjustments to display modal-alert instead of browser alert done by Matt Jennings
   This will get the loginMessage from the session if it exists, and then calls the showAlert() function to show the user a styled modal alert
*/
// Supress printing warnings/notices from declaring session_start() again -- this is necessary for $_SESSION to work in this file, but PHP complains regardless - Matt Jennings
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
session_start();
if (isset($_SESSION['loginBool']))
{
   if (isset($_SESSION["loginMessage"]) && $_SESSION["loginMessage"] != "") {
      // Display modal instead of alert
      print("<script>showAlert('". $_SESSION["loginTitle"] . "', '". $_SESSION["loginMessage"] . "')</script>");
      $_SESSION["loginMessage"] = "";
      if ($_SESSION['loginBool'] == false) {
        session_unset();
      }
    }
 }
 if (isset($_SESSION['loginBool']))
 {
    if (isset($_SESSION["loginMessage"]) && $_SESSION["loginMessage"] != "") {
       // Display modal instead of alert
       print("<script>showAlert('". $_SESSION["loginTitle"] . "', '". $_SESSION["loginMessage"] . "')</script>");
       $_SESSION["loginMessage"] = "";
       if ($_SESSION['loginBool'] == false) {
         session_unset();
       }
     }
  }
 ?>
