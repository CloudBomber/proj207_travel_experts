<!-- Matt Jennings / Kasi Emmanuel 

    This is the "home" section of the index page. This contains the package carousel slide with a button for the user to book the package.
-->
<?php
    require('php/classes/package.class.php');
    $dbh =mysqli_connect($host, $user, $password, $database);
    //put connection checking here
    if (!$dbh)
    {
        print(mysqli_connect_error());
    }
    $sql = "SELECT `PackageId`, `PkgName`, `PkgStartDate`, `PkgEndDate`, `PkgDesc`, `PkgBasePrice` FROM `packages` LIMIT 4 ";
    $result = mysqli_query($dbh, $sql);
    
    //check if result is there
    if (!$result)
    {
        mysqli_error($dbh);
    }
    
    // Used to determine which carosel item is active
    $active = false; 

    // Dates for the packages
    $startDate="";
    $endDate="";
    $startDateFormatted="";
    $endDateFormatted="";  

    // If the start/end date has passed the current date 
    $startDatePassed = false;
    $endDatePassed = false; 
?>

<div id="home">
    <!--Carousel Wrapper-->
    <div id="carousel-example-1" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-label">
            <p>UPCOMING PACKAGES</p>
        </div>
        <!--Indicators-->
        <ol class="carousel-indicators">   

            <!-- The list items generated here tell the carousel how many items there are -->         
            <?php
                $active = true;          
                $i = 0;                
                while ($row = mysqli_fetch_assoc($result)) {                                     
                    $package = new Package($row);
                                        
                    $endDate = new DateTime($package->getEndDate());                    
                    $endDateFormatted = date_format($endDate, 'M d Y');         
                    
                    // If the end date hasn't passed today's date, show the package
                    if (!(strtotime($endDateFormatted) < time())) {                                                                        
                        print("
                            <li data-target='#carousel-example-1' class=". ($active ? " active" : "") . " data-slide-to='$i'></li>
                        ");
                    }
                    $i++;
                    $active = false;
                } 
            ?>
        </ol>
        <!--/.Indicators-->

        <!--Slides-->
        <div class="carousel-inner" role="listbox">

            <!-- Generate the slides for the amount of available packages -->
          <?php
            // Since we've already run mysqli_fetch, we need to set the pointer back to 0 or else the while loop won't work
            mysqli_data_seek($result, 0);

            $active = true;            
            while ($row = mysqli_fetch_assoc($result)) {                                 
                $package = new Package($row);
                    
                // Date formatting and checking done by Matt JEnnings
                $startDate = new DateTime($package->getStartDate());
                $endDate = new DateTime($package->getEndDate());
                $startDateFormatted = date_format($startDate, 'M j Y');                 
                $endDateFormatted = date_format($endDate, 'M j Y');         

                $startDatePassed = false;
                if (strtotime($startDateFormatted) < time()) {
                    $startDatePassed = true;
                }

                // If the end date hasn't passed today's date, show the package
                if (!(strtotime($endDateFormatted) < time())) {                      
                    print("
                    <div class='carousel-item" . ($active ? " active" : "") . "'>

                        <div class='flex-center animated fadeInDown'>
                            <ul>
                                <li>
                                    <h1 class='h1-responsive'>{$package->getName()}</h1></li>
                                <li>
                                    <h2>\${$package->getBasePrice()}</h2>
                                    <p>{$package->getDescription()}</p>
                                    <h5> <span class='" . ($startDatePassed ? "date-passed" : "") . "'> $startDateFormatted</span> to $endDateFormatted </h5>
                                </li>
                                <li>
                                    <a role='button' class='btn btn-primary btn-lg' onclick='goToPackageDetails({$package->getId()})'>Book Now</a>
                                </li>
                            </ul>
                        </div>

                    </div>

                    ");
                }
                $active = false;
            }             
           ?>
            <!--Controls-->
            <a class="left carousel-control" href="#carousel-example-1" role="button" data-slide="prev">
                <span class="icon-prev" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-1" role="button" data-slide="next">
                <span class="icon-next" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        <!--/.Controls-->
        </div>
        <!--/.Carousel Wrapper-->
    </div>
</div>
<?php
mysqli_close($dbh);
?>
