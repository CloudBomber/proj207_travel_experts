<!-- Created by Kasi Emmanuel -->

<div id="about-us" class="container-fluid">
    <div class="container-fluid"><br/><br/>
      <div class="col-lg-12" id="Company">
          <h1 class="section-heading" align="center">About Us</h1>
          <div class="container-fluid" id="Company">
            <div class="col-lg-6">
            <section class="section company-section"
              align="left">
                <p class="section-description">
                  <h2>What we do</h2>
                  </p>
                <p>Lorem ipsum dolor sit amet, cu usu augue senserit, est stet laboramus ei.<br/> Veniam salutatus philosophia est ex, mel alii graeco maiorum ei,<br/>te mei possim scripserit.
                  </p><br/>
                <p>Lorem ipsum dolor sit amet, consul aperiri iuvaret usu an.<br/>Saperet deleniti nam cu, sit soluta deterruisset cu,<br/> cu cibo epicurei qui. Semper discere consectetuer vim eu. <br/>Esse consul mel ea, nec eu odio iriure accusata. Utinam verterem ex usu. <br/>Sed harum signiferumque te, cu eum timeam tamquam nostrum.</p>
            </section>
            </div>
            <div class="col-lg-6" id="img-1">
            </div>
          </div>
      </div>
      <div id="section1" class="content-slide">
        <div class="container-fluid" id="About-Us">
          <div class="col-lg-12" id="Works">
            <div class="col-lg-6" id="img-2">
            </div>
            <div class="col-lg-6">
              <section class="section process-section"
                  >
                <p class="section-description" >
                  <h2 align="center">How it works</h2>
                </p>
                <ul align="center">
                  <li>Purto concludaturque ius no, an vide nemore fabulas sed, <br/>choro tacimates sensibus mea an.<br/> Quo movet scaevola et, aperiri appetere ne ius.</li><br/>
                  <li>Inani dolores an vix. Eu duo dicam decore inimicus,<br/> mei no viris congue indoctum, nam eruditi ullamcorper ex.</li><br/>
                  <li>Eam alii sint vero ne, elit indoctum vis cu, <br/>veri platonem rationibus pri at.<br/>Eu per oblique delicata posidonium, meis suscipit vim ea.</li><br/>
                  <li>Ea iusto qualisque persecuti nec, ne debitis recteque vix.</li><br/>
                  <a class="btn btn-primary" onClick='scrollToId("#vacationpackages")' role="button">Plan My Trip</a>
                </ul>
              </section>
            </div>

          </div>
          <div class="col-lg-12" id="Money">
            <div class="col-lg-6">
              <section class="section quality-section"
                    align="left">
                  <p class="section-description">
                    <h2 align="center">How we make money</h2>
                  </p>
                  <p>Lorem ipsum dolor sit amet, cu usu augue senserit, est stet laboramus ei.<br/> Veniam salutatus philosophia est ex, mel alii graeco maiorum ei,<br/>te mei possim scripserit.</p>
                  <p>Oratio scaevola vix id. Cu fugit persequeris est, eros harum detracto vix ut.<br/> Tractatos temporibus cum ut. Impedit nostrum sea ei. <br/>Eu est ubique noluisse invenire, ei eam animal luptatum percipitur. <br/>Ea vim dicant putant insolens, quem purto consetetur nam at,<br/>ut vel postea ceteros voluptua.</p>
              </section>
            </div>
            <div class="col-lg-6" id="img-3">
            </div>
          </div>
          <div class="col-lg-12" id="Travel-tips" >
            <div class="col-lg-6" id="img-4">
            </div>
            <div class="col-lg-6">
              <section class="section
                  travel-tips-section"  align="center" >
                <p class="section-description">
                  <h2 align="center">Travel Tips</h2>
                </p>
                <ul >
                  <li>Purto concludaturque ius no, an vide nemore fabulas sed,<br/> choro tacimates sensibus mea an. Quo movet scaevola et,<br/>aperiri appetere ne ius.</li><br/>
                  <li>Inani dolores an vix. Eu duo dicam decore inimicus, <br/>mei no viris congue indoctum, nam eruditi ullamcorper ex.</li><br/>
                  <li>Eam alii sint vero ne, elit indoctum vis cu, veri platonem rationibus pri at.<br/> Eu per oblique delicata posidonium, meis suscipit vim ea.</li><br/>
                  <li>Ea iusto qualisque persecuti nec, ne debitis recteque vix.</li><br/>
                </ul>
                  <a class="btn btn-primary" onClick='scrollToId("#vacationpackages")' role="button">Plan My Trip</a>
              </section>
            </div>

          </div>
          <!--Section: Team v.1-->
          <div class="col-lg-12" id="Team" >
            <section class="section team-section">

              <!--Section heading-->
              <h1 class="section-heading" align = "center">Our amazing team</h1>
              <!--Section description-->
              <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, error amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam eum porro a pariatur accusamus veniam.</p>

              <!--First row-->
              <div class="row text-xs-center">

                  <!--First column-->
                  <div class="col-lg-3 col-md-6 mb-r">

                      <div class="avatar">
                          <img src="img/female1.jpg" class="rounded-circle">
                      </div>
                      <h4 style="padding: 10px;">Beverly Dease</h4>
                      <h5>Branch Manager</h5>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                  </div>
                  <!--/First column-->

                  <!--Second column-->
                  <div class="col-lg-3 col-md-6 mb-r">

                      <div class="avatar">
                          <img src="img/male1.jpg" class="rounded-circle">
                      </div>
                      <h4 style="padding: 10px;">Tom Simsons</h4>
                      <h5>Marketing Specialist</h5>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                  </div>
                  <!--/Second column-->

                  <!--Third column-->
                  <div class="col-lg-3 col-md-6 mb-r">

                      <div class="avatar">
                          <img src="img/female2.jpg" class="rounded-circle">
                      </div>

                      <h4 style="padding: 10px;">Stephanie Hang</h4>
                      <h5>Financial Manager</h5>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                  </div>
                  <!--/Third column-->

                  <!--Fourth column-->
                  <div class="col-lg-3 col-md-6 mb-r">

                      <div class="avatar">
                          <img src="img/male2.jpg" class="rounded-circle">
                      </div>
                      <h4 style="padding: 10px;">Travis Wells</h4>
                      <h5>Customer Service Manager</h5>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>


                  </div>
                  <!--/Fourth column-->

              </div>
              <!--/First row-->

            </section>
          </div>
          <!--/Section: Team v.1-->
        </div>
      </div>
    </div>
</div>
